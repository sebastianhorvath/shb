
CC = g++
CFLAGS = -g
LDLIBS= -L. -lspinapi -lm -ldl -lusb -lpthread -lrt -lz -lhdf5

.PHONY:	all clean install

all:	shb 

shb: shb.o vxi11_user.o vxi11_clnt.o vxi11_xdr.o
	$(CC) $(CFLAGS) $^ -o $@ $(LDLIBS)

shb.o: shb.c ../../vxi11/vxi11/vxi11_user.cc ../../vxi11/vxi11/vxi11.h
	$(CC) $(CFLAGS) -c $< -o $@ $(LDLIBS)

vxi11_user.o: ../../vxi11/vxi11/vxi11_user.cc ../../vxi11/vxi11/vxi11_user.h ../../vxi11/vxi11/vxi11.h
	$(CC) $(CFLAGS) -c $< -o $@

vxi11_clnt.o: ../../vxi11/vxi11/vxi11_clnt.c
	$(CC) $(CFLAGS) -c $^ -o $@

vxi11_xdr.o: ../../vxi11/vxi11/vxi11_xdr.c
	$(CC) $(CFLAGS) -c $^ -o $@

clean:
	rm -f *.o shb

install:
	cp -f shb /usr/local/bin

