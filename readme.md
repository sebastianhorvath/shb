Overview
========

Code for controlling a PulseBlasterII-DDS board driving a pair of AOMs to
conduct a spectral hole burning experiment.  Data acquisition is done using a
DPO7104 oscilloscope controlled over VXI-11 by using the VXI library for Linux
by Steve Sharples. 

Data is written to hdf5 files. 

The current implementation is probably not very useful for distribution and is
primarily here for backup.  Parameters are hard-coded and there's no convenient
interface.  At some point (after thesis submission), I intend to add a textfile
based input, at which point this program may be more useful to others.   
