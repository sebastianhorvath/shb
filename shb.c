/*
    Copyright (C) 2015 Sebastian Horvath (sebastian.horvath@gmail.com)
 
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <unistd.h>


#include "hdf5.h"
#include "spinapi.h"
#include "vxi11/vxi11_user.h"

/* Pulse blaster specific definitions follow. */
// This value must be 75 MHz for standard PBDDS-II-300 boards
#define CLOCK_FREQ 75.0  

/* Human readable names for the control bits. */
#define TX_ENABLE 1
#define TX_DISABLE 0
#define PHASE_RESET 1
#define NO_PHASE_RESET 0
#define FREQ0 0
#define FREQ1 1
#define AMP0 0
#define AMP1 1
#define PHASE0 0
#define PHASE1 1

#define START_LOCATION (0x40000+0x07)
#define FLAG_STATES (0x40000+0x08)

/* The burn pulse AOM modulation frequency. */
#define PULSE_FREQ 80*MHz
/* The chirp pulse amplitude modulation. */
#define CHIRP_AMP 0.15
/* Risetimes for AWG and DL100 electronics. */
#define DL100_AWG_RISET 112*us
/* Risetime for gating AOM and electronics. */
#define GATE_AOM_RISET 25*us

/* DPO data acquisition specific definitions follow. */
/* Timeout for device comms; in ms. 
 *
 * NB. the current device timeout scheme is pretty hairy.  In particular, there
 * is an issue for vxi11_receive, defined in vxi11_user.cc, for data collection
 * exceeding 25 seconds per vxi11_receive call.  The TIMEOUT value defined here
 * is passed to the vxi11 library, which assigns it to a struct,
 * Device_ReadParms, defined in vxi11_user.h.  The Device_ReadParams struct is
 * passed to an RPC clnt_call (via device_write_1, defined in vxi11_cInt.c).
 * The clnt_call wraps the RPC ioctl() and appears to be specific to the
 * installed RPC implementation (see /usr/include/rpc/clnt.h).  With RPC
 * provided by glibc-headers-2.12-1.149.el6.x86_64, the device times out after
 * 25 seconds, irrespective of the value set in Device_ReadParams.  However,
 * there is also a default timeout defined in vxi11_cInt.c.  It appears that
 * this is the value that is ultimatly used.  vxi11_cInt.c is created by
 * rpcgen, and therefore it is advised that the default timeout should be
 * adjusted using an RPC clnt_control call, rather than editing vxi11_cInt.c.
 * According to the RPC specification, this would set the timeout for all
 * future clnt_call calls, irrespective of their individually set timeout.  It
 * is not clear to me what happens to the timeout value set by
 * Device_ReadParams.  For now, I simply ignored the warning to change the
 * default timeout value with clnt_control, and adjusted the TIMEOUT value from
 * { 25, 0 } to { 105, 0 } in vxi11_cInt.c. */
#define TIMEOUT 20000
/* Maximum number of channels. */
#define MAX_NUM_CH 6
/* Maximum length of channel names. */
#define MAX_LEN_CH 10

int init_pb();
int calc_pulse_seq_delay(float rr, float pt, float cdelay, float ct, float *delay, float *dl100_awg_offset);
void start_dl100_chirp_pulse_seq(float pt, float pa, float cdelay, float ct, float rr, float delay, float dl100_awg_offset);
void start_aom_fm_pulse_seq(float pt, float pa, float cdelay, float ct, float rr, float delay, float dl100_awg_offset, float fc, float fm, float am);
void start_eom_fm_pulse_seq(float pt, float pa, float cdelay, float ct, float rr, float delay, float dl100_awg_offset, float eom_amp);

typedef struct {
  /* Each element corresponds to a char buffer used for a specific oscilloscope
   * channel. */
  char **buf_a;
  /* The length of the channel char buffers. */
  long buf_len;
  /* Array of strings specifying the enabled channels. */
  char **ch_a;
  /* The length of ch_a; that is, the number of enabled channels. */
  int n_ch;
  /* The number of averages the scope will collect. */
  long n_avgs;
  /* The time required to complete the acquisition of all the samples. */
  double acq_time;
} acq_data;

typedef struct {
  /* Number of waveforms. */
  long n;
  /* Length of individual waveforms. */
  long len;
  /* Waveform data arrays. */
  short *wfm;
} wfm_data;

int init_dpo_acq(char *ip, CLINK *clink);
acq_data *setup_dpo_acq(CLINK *clink, char *ch, int n_ch, int n_avgs, double reprate);
void free_dpo_acq(acq_data *acq);
long run_dpo_acq(CLINK *clink, acq_data *acq);
void parse_wfm_buf(acq_data *acq, long no_bytes, wfm_data *wfm_d, int *mask);
wfm_data *alloc_wfm(int n, acq_data *acq);
void write_dpo_meta(CLINK *clink, FILE *meta_f, acq_data *acq, long no_of_bytes);
void free_wfm(wfm_data *wfm_d);

typedef struct {
  /* hdf5 file handle. */
  hid_t file;
  /* hdf5 dataspace handle. */
  hid_t dataspace;
  /* dataset dimensions. */
  hsize_t dims[2];
} hdf5_data;

hdf5_data *hdf5_open(char *filename, long nrow, long ncol); 
void hdf5_close(hdf5_data *hdf5_d);
void hdf5_write_scan(hdf5_data *hdf5_d, char *id, short *data);
void hdf5_write_meta(hdf5_data *hdf5_d, char *notes);

/* 
 * Initialize the Pulse Blaster board.  This must be called prior to any
 * start_*_*_pulse_seq function.  Assumes only a single pulse blaster board is
 * connected to the system. 
 *
 * Returns
 * -------
 * retval	0 if the pulse blaster is initialized successfully; -1 otherwise. 
 */ 
int init_pb() {
  if (pb_count_boards() <= 0) {
    printf("Received the following error during pulse blaster initialization: unable to detect board. Exiting.\n");
    return -1;
  }
  else if (pb_init() != 0) {
    printf("Received the following error during pulse blaster initialization: %s. Exiting.\n", pb_get_error());
    return -1;
  }

  return 0;
}

/* 
 * Calculate the required delay at the end of a pulse sequence for a given rep
 * rate, pulse length, chirp delay and chirp time.  Additionally, ensure
 * specified parameters are possible given the risetime of the ramp function,
 * assuming the ramp function linearly sweeps the DL100 50% of the period. 
 *
 * Parameters
 * ----------
 * rr			The repetition rate.
 * pt  			The pulse duration in ms.
 * cdelay		The chirp delay in ms.
 * ct			The chirp duration in ms.
 * delay		ptr to variable which will be overwritten with the delay time upon exit.
 * dl100_awg_offset	ptr to variable that will be overwritten with the total offset between 
 * 			the start of the linear ramp and the trigger required for the DL100 AWG.
 */
int calc_pulse_seq_delay(float rr, float pt, float cdelay, float ct, float *delay, float *dl100_awg_offset) {
  float rp, rrt;

  /* Assuming ramp symmetry of 50%, we find the ramp period. */
  rp = ct/0.5;
  /* The ramp rise time. */
  rrt = (rp - ct)/2.0;
  /* Timing offset for DL100 AWG trigger. */
  *dl100_awg_offset = rrt+DL100_AWG_RISET;

  *delay = 1000/rr - (pt+cdelay+ct);
  if (*delay < 0) {
    printf("Error: pulse sequence duration exceeds the period of one repetition set by rep rate. Sequence time=%f, rep. rate period=%f.\n", (pt+cdelay+ct), 1000/rr);
    return -1;
  }
  else if (cdelay < rrt) {
    printf("Error: ramp rise-time exceeds the currently set chirp-delay.");
    return -1;
  }
  else if (GATE_AOM_RISET > *dl100_awg_offset) {
    printf("The GATE_AOM_RISET is set to a value greater than the value of dl100_awg_offset; the current program cannot handle this scenario.");
    return -1;
  }
  printf("Set the DL100 AWG ramp period to: %.10f ms\n", rp/1000000);

  return 0;
}


/*
 * Pulse sequence consists of the following steps:
 *   + burn hole using AOM controlled by pb AWG channel 1;
 *   + generate chirp by sweeping DL100 using an AWG with suitable ramp triggered by FLAG2;
 *   + additionally attenuate the laser during the chirp using AOM on pb AWG channel 1;
 *   + gate detector with second AOM triggered by FLAG1;
 *   + FLAG0 is set high at the beginning of the linear section of the DL100 chirp; used to trigger DPO7104. 
 *
 * NB: pb AWG channel 2 is not used for this pulse sequence. 
 *
 * Parameters
 * ----------
 * pt			Burn pulse duration, in ms.
 * pa			Burn pulse amplitude, between 0 and 1.
 * cdelay		Delay between burn pulse and chirp.
 * ct			Chirp pulse duration, in ms.
 * rr			Pulse sequence repetition rate.
 * delay		Delay required at the end of the pulse sequence; calculate with calc_pulse_seq_delay.
 * dl100_awg_offset	Total offset between the start of the linear ramp and the trigger required for the 
 * 			DL100 AWG; calculate with calc_pulse_seq_delay.
 */
void start_dl100_chirp_pulse_seq(float pt, float pa, float cdelay, float ct, float rr, float delay, float dl100_awg_offset) {
  int start;

  pb_core_clock(CLOCK_FREQ); 

  /* DDS0 frequency, phase and amplitude register programing. */   
  pb_select_dds(0); 

  pb_start_programming(FREQ_REGS); 
  pb_set_freq(PULSE_FREQ); 
  pb_stop_programming();

  pb_start_programming(TX_PHASE_REGS); 
  pb_set_phase(0.0); 
  pb_stop_programming();

  pb_set_amp(pa,0); 
  pb_set_amp(CHIRP_AMP,1);

  /* DDS1 frequency, phase and amplitude register programing.  This channel is
   * unused in this pulse sequence. */   
  pb_select_dds(1); 

  pb_start_programming(FREQ_REGS); 
  pb_set_freq(2*MHz); 
  pb_stop_programming();

  pb_start_programming(TX_PHASE_REGS); 
  pb_set_phase(0.0); 
  pb_stop_programming();

  pb_set_amp(1,0); 

  /* Specify the pulse program. */
  pb_start_programming(PULSE_PROGRAM);  
  start = pb_inst_dds2(FREQ0, PHASE0, AMP0, TX_ENABLE, NO_PHASE_RESET, FREQ0, PHASE0, AMP0, TX_DISABLE, NO_PHASE_RESET, 0x008, CONTINUE, 0, pt);
  pb_inst_dds2(FREQ0, PHASE0, AMP0, TX_DISABLE, NO_PHASE_RESET, FREQ0, PHASE0, AMP0, TX_DISABLE, NO_PHASE_RESET, 0x000, CONTINUE, 0, cdelay-dl100_awg_offset-GATE_AOM_RISET);
  pb_inst_dds2(FREQ0, PHASE0, AMP0, TX_DISABLE, NO_PHASE_RESET, FREQ0, PHASE0, AMP0, TX_DISABLE, NO_PHASE_RESET, 0x004, CONTINUE, 0, dl100_awg_offset-GATE_AOM_RISET);
  pb_inst_dds2(FREQ0, PHASE0, AMP0, TX_DISABLE, NO_PHASE_RESET, FREQ0, PHASE0, AMP0, TX_DISABLE, NO_PHASE_RESET, 0x006, CONTINUE, 0, GATE_AOM_RISET);
  pb_inst_dds2(FREQ0, PHASE0, AMP1, TX_ENABLE, NO_PHASE_RESET, FREQ0, PHASE0, AMP0, TX_DISABLE, NO_PHASE_RESET, 0x007, CONTINUE, 0, ct);
  pb_inst_dds2(FREQ0, PHASE0, AMP0, TX_DISABLE, PHASE_RESET, FREQ0, PHASE0, AMP0, TX_DISABLE, NO_PHASE_RESET, 0x000, BRANCH, start, delay);

  pb_stop_programming();  

  pb_write_register (START_LOCATION, 0);
  pb_write_register (FLAG_STATES, 0);

  /* Send a software trigger to begin execution of the pulse sequence. */	
  pb_start();
}

/* 
 * Pulse sequence consists of the following steps:
 *   + burn hole using AOM controlled by pb AWG channel 1;
 *   + generate chirp by sweeping DL100 using an AWG with suitable ramp triggered by FLAG2;
 *   + additionally attenuate and FM modulate the laser during the chirp using AOM on pb AWG channel 1;
 *   + gate detector with second AOM triggered by FLAG1;
 *   + FLAG0 is set high at the beginning of the linear section of the DL100 chirp; used to trigger DPO7104. 
 * 
 * Notes: pb AWG channel 2 is not used for this pulse sequence. 
 *
 * Parameters
 * ----------
 * pt			Burn pulse duration, in ms.
 * pa			Burn pulse amplitude, between 0 and 1.
 * cdelay		Delay between burn pulse and chirp.
 * ct			Chirp pulse duration, in ms.
 * rr			Pulse sequence repetition rate.
 * delay		Delay required at the end of the pulse sequence; calculate with calc_pulse_seq_delay.
 * dl100_awg_offset	Total offset between the start of the linear ramp and the trigger required for the 
 * 			DL100 AWG; calculate with calc_pulse_seq_delay.
 * fc			FM carrier frequency.
 * fm			FM modulation frequency.
 * am			FM modulation amplitude.
 */
void start_aom_fm_pulse_seq(float pt, float pa, float cdelay, float ct, float rr, float delay, float dl100_awg_offset, float fc, float fm, float am) {
  int start, fm_loop;
  int i, n_loop_fmp, n_partial_fmp;
  float fmdt, fmp;  
  float chirp_freq[1022];

  pb_core_clock(CLOCK_FREQ); 

  /* DDS0 frequency, phase and amplitude register programing. */   
  pb_select_dds(0); 

  pb_start_programming(FREQ_REGS); 
  pb_set_freq(PULSE_FREQ); 
  pb_set_freq(2*MHz);

  /* We populate all but two of the available frequency registers with FM
   * modulated frequencies. The speed at which we step through these registers
   * determines the modulation frequency. */
  for (i=0; i<1022; i++) {
    chirp_freq[i] = fc + am*sin(2.0*M_PI* (float )i/1022); 
    pb_set_freq(chirp_freq[i]);
  }

  /* Time increment for fm modulation; factor of 1000 accounts for different
   * definitions for time and frequency units used by the spinapi. */
  fmdt = 1000.0/(1022.0*fm);

  /* Duration of a singe FM period. */
  fmp = 1000.0/fm;
  /* Number of complete FM periods in chirp time. */
  n_loop_fmp = (int) floorf(ct/fmp);
  if (n_loop_fmp < 1) {
    printf("Warning: current FM parameters will result in an FM period less than the chirp time; this is unsupported.\n");
  }
  else {
    printf("There are %i complete fm periods during the chirp.\n", n_loop_fmp);
  }
  /* Nearest integer number of fmdt increments in remainder of ct/fmp. */
  n_partial_fmp = lrintf((ct-n_loop_fmp*fmp)/fmdt);

  pb_stop_programming();

  pb_start_programming(TX_PHASE_REGS); 
  pb_set_phase(0.0); 
  pb_stop_programming();

  pb_set_amp(pa,0); 
  pb_set_amp(CHIRP_AMP,1);

  /* DDS1 frequency, phase and amplitude register programing.  This channel is
   * unused in this pulse sequence. */   
  pb_select_dds(1); 

  pb_start_programming(FREQ_REGS); 
  pb_set_freq(2*MHz); 
  pb_stop_programming();

  pb_start_programming(TX_PHASE_REGS); 
  pb_set_phase(0.0); 
  pb_stop_programming();

  pb_set_amp(1,0); 

  /* Specify the pulse program. */
  pb_start_programming(PULSE_PROGRAM);  

  start = pb_inst_dds2(FREQ0, PHASE0, AMP0, TX_ENABLE, NO_PHASE_RESET, FREQ0, PHASE0, AMP0, TX_DISABLE, NO_PHASE_RESET, 0x008, CONTINUE, 0, pt);
  pb_inst_dds2(FREQ0, PHASE0, AMP0, TX_DISABLE, NO_PHASE_RESET, FREQ0, PHASE0, AMP0, TX_DISABLE, PHASE_RESET, 0x000, CONTINUE, 0, cdelay-dl100_awg_offset-GATE_AOM_RISET);
  pb_inst_dds2(FREQ0, PHASE0, AMP0, TX_DISABLE, NO_PHASE_RESET, FREQ0, PHASE0, AMP0, TX_DISABLE, PHASE_RESET, 0x004, CONTINUE, 0, dl100_awg_offset-GATE_AOM_RISET);
  pb_inst_dds2(FREQ0, PHASE0, AMP0, TX_DISABLE, NO_PHASE_RESET, FREQ0, PHASE0, AMP0, TX_DISABLE, PHASE_RESET, 0x006, CONTINUE, 0, GATE_AOM_RISET);

  /* Loop over all complete FM periods contained in the chirp duration. */
  fm_loop = pb_inst_dds2(2, PHASE0, AMP1, TX_ENABLE, NO_PHASE_RESET, FREQ0, PHASE0, AMP0 , TX_ENABLE, NO_PHASE_RESET, 0x007, LOOP, n_loop_fmp, fmdt);
  for (i=0; i<1020; i++) {
    pb_inst_dds2(i+3, PHASE0, AMP1, TX_ENABLE, NO_PHASE_RESET, FREQ0, PHASE0, AMP0 , TX_ENABLE, NO_PHASE_RESET, 0x007, CONTINUE, 0, fmdt);
  }
  pb_inst_dds2(1021, PHASE0, AMP1, TX_ENABLE, NO_PHASE_RESET, FREQ0, PHASE0, AMP0 , TX_ENABLE, NO_PHASE_RESET, 0x007, END_LOOP, fm_loop, fmdt);
  /* Create FM modulation instructions for the remainder of the chirp time. */
  for (i=0; i<n_partial_fmp; i++) {
    pb_inst_dds2(i+2, PHASE0, AMP1, TX_ENABLE, NO_PHASE_RESET, FREQ0, PHASE0, AMP0 , TX_ENABLE, NO_PHASE_RESET, 0x007, CONTINUE, 0, fmdt);
  }

  pb_inst_dds2(FREQ0, PHASE0, AMP0, TX_DISABLE, PHASE_RESET, FREQ0, PHASE0, AMP0, TX_DISABLE, PHASE_RESET, 0x000, BRANCH, start, delay);

  pb_stop_programming(); 

  pb_write_register(START_LOCATION, 0);
  pb_write_register(FLAG_STATES, 0);

  /* Send a software trigger to begin execution of the pulse sequence. */	
  pb_start();
}

/*
 * Pulse sequence consists of the following steps:
 *   + burn hole using AOM controlled by pb AWG channel 1;
 *   + generate chirp by sweeping DL100 using an AWG with suitable ramp triggered by FLAG2;
 *   + additionally attenuate the laser during the chirp using AOM on pb AWG channel 1;
 *   + FM modulate the chirp at 20 MHz, using the EOM on pb AWG channel 2;
 *   + gate detector with second AOM triggered by FLAG1;
 *   + FLAG0 is set high at the beginning of the linear section of the DL100 chirp; used to trigger DPO7104. 
 *
 *
 * Parameters
 * ----------
 * pt			Burn pulse duration, in ms.
 * pa			Burn pulse amplitude, between 0 and 1.
 * cdelay		Delay between burn pulse and chirp.
 * ct			Chirp pulse duration, in ms.
 * rr			Pulse sequence repetition rate.
 * delay		Delay required at the end of the pulse sequence; calculate with calc_pulse_seq_delay.
 * dl100_awg_offset	Total offset between the start of the linear ramp and the trigger required for the 
 * 			DL100 AWG; calculate with calc_pulse_seq_delay.
 * eom_amp		The amplitude with which to drive the EOM; determines the modulation depth (typically something like 0.6).
 */
void start_eom_fm_pulse_seq(float pt, float pa, float cdelay, float ct, float rr, float delay, float dl100_awg_offset, float eom_amp) {
  int start;

  pb_core_clock(CLOCK_FREQ); 

  /* DDS0 frequency, phase and amplitude register programing. */   
  pb_select_dds(0); 

  pb_start_programming(FREQ_REGS); 
  pb_set_freq(PULSE_FREQ); 
  pb_stop_programming();

  pb_start_programming(TX_PHASE_REGS); 
  pb_set_phase(0.0); 
  pb_stop_programming();

  pb_set_amp(pa,0); 
  pb_set_amp(CHIRP_AMP,1);

  /* DDS1 frequency, phase and amplitude register programing.  This channel is
   * unused in this pulse sequence. */   

  pb_select_dds(1);

  pb_start_programming(FREQ_REGS); 
  pb_set_freq(20.0*MHz);
  pb_stop_programming();

  pb_start_programming(TX_PHASE_REGS); 
  pb_set_phase(0.0); 
  pb_stop_programming();

  pb_set_amp(eom_amp,0); 

  /* Specify the pulse program. */
  pb_start_programming(PULSE_PROGRAM);  
  start = pb_inst_dds2(FREQ0, PHASE0, AMP0, TX_ENABLE, NO_PHASE_RESET, FREQ0, PHASE0, AMP0, TX_DISABLE, NO_PHASE_RESET, 0x008, CONTINUE, 0, pt);
  pb_inst_dds2(FREQ0, PHASE0, AMP0, TX_DISABLE, NO_PHASE_RESET, FREQ0, PHASE0, AMP0, TX_DISABLE, NO_PHASE_RESET, 0x000, CONTINUE, 0, cdelay-dl100_awg_offset-GATE_AOM_RISET);
  pb_inst_dds2(FREQ0, PHASE0, AMP0, TX_DISABLE, NO_PHASE_RESET, FREQ0, PHASE0, AMP0, TX_ENABLE, NO_PHASE_RESET, 0x004, CONTINUE, 0, dl100_awg_offset-GATE_AOM_RISET);
  pb_inst_dds2(FREQ0, PHASE0, AMP0, TX_DISABLE, NO_PHASE_RESET, FREQ0, PHASE0, AMP0, TX_ENABLE, NO_PHASE_RESET, 0x006, CONTINUE, 0, GATE_AOM_RISET);
  pb_inst_dds2(FREQ0, PHASE0, AMP1, TX_ENABLE, NO_PHASE_RESET, FREQ0, PHASE0, AMP0, TX_ENABLE, NO_PHASE_RESET, 0x007, CONTINUE, 0, ct);
  pb_inst_dds2(FREQ0, PHASE0, AMP0, TX_DISABLE, PHASE_RESET, FREQ0, PHASE0, AMP0, TX_DISABLE, PHASE_RESET, 0x000, BRANCH, start, delay);

  pb_stop_programming();  

  pb_write_register (START_LOCATION, 0);
  pb_write_register (FLAG_STATES, 0);

  /* Send a software trigger to begin execution of the pulse sequence. */	
  pb_start();
}


int init_dpo_acq(char *ip, CLINK *clink) {
  int ret;

  vxi11_open_device(ip, clink);

  /* No headers in replies. */
  ret = vxi11_send(clink, ":HEADER 0"); 
  /* Ensure communication with device succeeded. */
  if (ret < 0) {
    printf("Error: failed to send command ':HEADER 0' in init_dpo_acq.\n");
    return ret;
  }

  /* 2 bytes per data point (16 bit). */
  vxi11_send(clink, ":DATA:WIDTH 2"); 
  /* Little endian, signed. */
  vxi11_send(clink, ":DATA:ENCDG SRIBINARY"); 

  return 0;
}

/* Call to configure the scope for acquisition, and return an acquisition struct
 * to be passed run_dpo_acq.  This needs to be run prior to calling
 * run_dpo_acq, but multiple acquisitions can be run without re-running the
 * setup. 
 *
 * Parameters
 * ----------
 * clink	client link struct. 
 * ch		String specifying which channels are enabled.  Must be of the
 * 		form: CH1,CH2,CH3,CH4.  
 * n_avgs	The number of averages to take; must be greater or equal to 2. 
 * reprate	The experimental repetition rate.  Must be in pulse blaster
 * 		units of frequency. 
 */
acq_data *setup_dpo_acq(CLINK *clink, char *ch, int n_avgs, double reprate) {
  char cmd[256];
  int n_ch, i, j;
  long no_acq_points, no_points, start, stop;
  double sample_rate, hor_scale;
  acq_data *acq;

  acq = (acq_data *) malloc(sizeof(acq_data));
  if (acq == 0) {
    printf("Error: malloc failed for acq_data in setup_dpo_acq.\n");
    return NULL;
  }

  /* Determine how many channels are enabled. */
  n_ch = 0;
  acq->ch_a = (char **) malloc(sizeof(char *)*MAX_NUM_CH);
  for (char *p = strtok(ch, ","); p; p = strtok(NULL, ",")) {
    if (n_ch >= MAX_NUM_CH) {
      printf("Error: maximum number of channels exceeded; adjust MAX_NUM_CH.\n");
      break;
    }
    acq->ch_a[n_ch] = (char *) malloc(sizeof(char)*MAX_LEN_CH);
    if (acq->ch_a[n_ch] == 0) {
      for (i=0; i<n_ch; i++) {
        free(acq->ch_a[i]);
      }
      free(acq->ch_a);
      free(acq);
      printf("Error: malloc failed for acq->ch_a.\n");
    }
    strcpy(acq->ch_a[n_ch++], p);
  }

  /* Calculate the number of expected data points given what is on the scope
   * screen, then adjust the DATA start and stop values to match this
   * expectation.  This ensures no extra data is returned if the scope samples
   * over a larger window than the horizontal window being displayed.
   * tek_scope_calculate_no_of_bytes has a more detailed explanation. */ 
  no_acq_points = vxi11_obtain_long_value(clink, "HOR:RECORD?");
  hor_scale = vxi11_obtain_double_value(clink, "HOR:MAIN:SCALE?");

  sample_rate = vxi11_obtain_double_value(clink, "HOR:MAIN:SAMPLERATE?");
  no_points = (long) round(sample_rate*10*hor_scale);

  start = ((no_acq_points - no_points)/2) + 1;
  stop  = ((no_acq_points + no_points)/2);

  /* Set number of points to receive to be equal to the record length */
  sprintf(cmd, "DATA:START %ld", start);
  vxi11_send(clink, cmd);
  sprintf(cmd, "DATA:STOP %ld", stop);
  vxi11_send(clink, cmd);

  /* Buffer length is 2 bytes per point per channel.  We record the per channel
   * buffer length separately, as we need it for vxi11_receive_data_block. */
  acq->buf_len = 2*no_points;
  acq->buf_a = (char **) malloc(n_ch*sizeof(char *));
  if (acq->buf_a == 0) {
    for (i=0; i<n_ch; i++) {
      free(acq->ch_a[i]);
    }
    free(acq->ch_a);
    free(acq);
    printf("Error: malloc failed for acq->buf_a in setup_dpo_acq.\n");
    return NULL;
  }
  for (i=0; i<n_ch; i++) {
    acq->buf_a[i] = (char *) malloc(acq->buf_len*sizeof(char));
    if (acq->buf_a[i] == 0) {
      for (j=0; j<i; j++) {
        free(acq->buf_a[i]);
      }
      for (j=0; j<n_ch; j++) {
        free(acq->ch_a[j]);
      }
      free(acq->ch_a);
      free(acq);
      printf("Error: malloc failed for acq->buf_a[i] in setup_dpo_acq.\n");
      return NULL;
    }
  }

  /* Set acquisition mode to clear sweeps. */
  vxi11_send(clink, "ACQUIRE:STOPAFTER SEQUENCE");

  /* Setup averaging. */
  sprintf(cmd, "ACQUIRE:NUMAVG %d", n_avgs);
  vxi11_send(clink, cmd);
  vxi11_send(clink, "ACQUIRE:MODE AVERAGE");

  /* Assumes that the reprate is slow enough that any data transfer latency is
   * negligible compared to the relaxation period after the end of the pulse
   * sequence.  The 1e-3 factor is for conversion from pulseblaster frequency
   * units to ms. */
  acq->acq_time = (1.0e-3/reprate)*n_avgs;
  acq->n_ch = n_ch;
  acq->n_avgs = n_avgs;

  return acq;
}

void free_dpo_acq(acq_data *acq) {
  int i;
  for (i=0; i<acq->n_ch; i++) {
    free(acq->buf_a[i]);
    free(acq->ch_a[i]);
  }
  free(acq->buf_a);
  free(acq->ch_a);
  free(acq);
}

/* Run acquisition according to configuration created with setup_dpo_acq, which
 * must be called at least once prior to running this function. 
 *
 * Parameters
 * ----------
 * clink	client link struct. 
 * acq		Acquisition data.
 *
 */
long run_dpo_acq(CLINK *clink, acq_data *acq) {
  char cmd[256];
  int ret, i;
  long bytes_returned, opc_value;

  /* This is the equivalent of pressing the "Single" button on the scope. */
  vxi11_send(clink, "ACQUIRE:STATE 1");
  /* This request will not return ANYTHING until the acquisition is complete
   * (OPC? = OPeration Complete?). */
  opc_value = vxi11_obtain_long_value(clink, "*OPC?", acq->acq_time+TIMEOUT);
  if (opc_value != 1) {
    printf("Error: OPC? request returned %ld, (should be 1).  Either a timeout problem or the scope is in a sad place; try increasing TIMEOUT or a scope restart.\n", opc_value);
    return -1;
  }
  for (i=0; i<acq->n_ch; i++) {
    /* Set the source channel. */
    sprintf(cmd, "DATA:SOURCE %s", acq->ch_a[i]);
    ret=vxi11_send(clink, cmd);
    if (ret < 0) {
      printf("Error: failed to send command 'DATA:SOURCE' in run_dpo_acq (returned %i).\n", ret);
      return ret;
    }
    /* Ask for the data, and receive it. */
    vxi11_send(clink, "CURVE?");

    bytes_returned=vxi11_receive_data_block(clink, acq->buf_a[i], acq->buf_len, TIMEOUT);
  }

  return bytes_returned;
}

/* 
 * Parse the waveform byte data from the scope into a two dimensional short
 * array.  The mask array determines which entry in wf_data a particular buf
 * entry is parsed into.  This is useful for saving the reference data channel
 * along with the hole-burning data in a single wf_data array. 
 *
 * Parameters
 * ----------
 * acq		Acquisition data.
 * wfm_d	Waveform data allocated with wfm_data_alloc. 
 * mask		Determines which array entry in wfm_d->wfm that a particular
 * 		channel is written to. An entry less than zero is dropped.
 */
void parse_wfm_buf(acq_data *acq, long no_bytes, wfm_data *wfm_d, int *mask) {
  int ch, i;
  /* Parse two bytes into signed short integers. */
  for (ch=0; ch<acq->n_ch; ch++) {
    if (mask[ch] >= 0) {
      for (i=0; i<wfm_d->len; i++) {
        wfm_d->wfm[mask[ch]*(wfm_d->len)+i] = (short)((unsigned char) acq->buf_a[ch][i*2+1] << 8 | (unsigned char) acq->buf_a[ch][i*2]);
      }
    }
  }
} 

/* Allocate space for waveform data to be written to hdf5 file. 
 *
 * Parameters
 * ----------
 *  n	  The number of channels recorded in wfm_data.
 *  acq	  The corresponding acquisition data object.
 */
wfm_data *alloc_wfm(int n, acq_data *acq) {
  wfm_data *wfm_d;

  wfm_d = (wfm_data *) malloc(sizeof(wfm_data));
  if (wfm_d == 0) {
    printf("Error: malloc failed for wfm_d in alloc_wfm.\n");
    return NULL;
  }
  wfm_d->wfm = (short *) malloc(n*(acq->buf_len)/2*sizeof(short));
  if (wfm_d->wfm == 0) {
    free(wfm_d);
    printf("Error: malloc failed for wfm_d->wfm in alloc_wfm.\n");
  }
  wfm_d->n = n;
  wfm_d->len = acq->buf_len/2;

  return wfm_d;
}


void free_wfm(wfm_data *wfm_d) {
  free(wfm_d->wfm);
  free(wfm_d);
}

/* Create hdf5 file and dataspace types for numerical data.
 *
 * Parameters
 * ----------
 * filename     The filename of the hdf5 file to be created; must be unique
 * nrow 	The number of rows of data to be written. 
 * ncol 	The number of columns of data to be written. 
 */
hdf5_data *hdf5_open(char *filename, long nrow, long ncol) {
  hdf5_data *hdf5_d;

  hdf5_d = (hdf5_data *) malloc(sizeof(hdf5_data));
  if (hdf5_d == 0) {
    printf("Error: malloc failed for hdf5_data in hdf5_open.\n");
    return NULL;
  }
  /* Change H5F_ACC_EXCL to H5F_ACC_TRUNC to enable file overwrite.*/
  hdf5_d->file = H5Fcreate(filename, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);

  hdf5_d->dims[0] = nrow;
  hdf5_d->dims[1] = ncol;
  hdf5_d->dataspace = H5Screate_simple(2, hdf5_d->dims, NULL);

  return hdf5_d;
}

void hdf5_close(hdf5_data *hdf5_d) {
  H5Sclose(hdf5_d->dataspace);
  H5Fclose(hdf5_d->file);
  free(hdf5_d);
}


/* Write a two dimensional array to an hdf5 dataset. 
 *
 * Parameters
 * ----------
 *  hdf5_d	hdf5 parameter data, created with hdf5_open.
 *  id		Identifier for this dataset.
 *  data	Two dimensional array of shorts.
 */ 
void hdf5_write_scan(hdf5_data *hdf5_d, char *id, short *data) {
  hid_t dataset;
  herr_t ret;

  dataset = H5Dcreate2(hdf5_d->file, id,  H5T_NATIVE_SHORT, hdf5_d->dataspace, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
  ret = H5Dwrite(dataset, H5T_NATIVE_SHORT, H5S_ALL, H5S_ALL, H5P_DEFAULT, data);

  H5Dclose(dataset);
}

/*
 * Write metadata of a scan to the hdf5 file set by hdf5_data. 
 *
 * Parameters
 * ----------
 * hdf5_d	hdf5 parameter data, created with hdf5_open.
 * clink	Oscilloscope client link struct. 
 * acq		Acquisition data.
 * notes        String of additional notes relevant to the scan, such as burn
 *              and probe beam power, laser polarization w.r.t. sample
 *              orientation, etc.
 */
void hdf5_write_meta(hdf5_data *hdf5_d, CLINK *clink, acq_data *acq, char *notes) {
  int i;
  hid_t str_ds, int_ds;
  hid_t str_attr, int_attr;
  hid_t str_t;
  herr_t ret;
  char str_buf[256];
  int timeout;
  double ch_pos, ch_scale;
  double ymult, yzero, yoff;
  double hor_recordlength, hor_samplerate, xzero, hor_interval, pt_off;
  char enc_desc[] = "Waveforms are stored as 16 bit integers. Absolute coordinates are given by y = yzero + ymult (yn - yoff) and x = xzero + hor_interval (xn - pt_off); yn are waveform points, and xn ranges, inclusively, from 0 to hor_recordlength-1.";

  str_ds = H5Screate(H5S_SCALAR);
  str_t = H5Tcopy(H5T_C_S1);

  H5Tset_size(str_t, strlen(notes));
  H5Tset_strpad(str_t,H5T_STR_NULLTERM);
  str_attr = H5Acreate2(hdf5_d->file, "notes", str_t, str_ds, H5P_DEFAULT, H5P_DEFAULT);
  ret = H5Awrite(str_attr, str_t, notes);
  ret = H5Aclose(str_attr);

  H5Tset_size(str_t, strlen(enc_desc));
  H5Tset_strpad(str_t,H5T_STR_NULLTERM);
  str_attr = H5Acreate2(hdf5_d->file, "encoding", str_t, str_ds, H5P_DEFAULT, H5P_DEFAULT);
  ret = H5Awrite(str_attr, str_t, enc_desc);
  ret = H5Aclose(str_attr);

  ret = H5Sclose(str_ds);
  ret = H5Tclose(str_t);

  hor_recordlength = vxi11_obtain_double_value(clink, "HOR:MODE:RECO?");
  hor_samplerate = vxi11_obtain_double_value(clink, "HOR:MODE:SAMPLER?");
  hor_interval = vxi11_obtain_double_value(clink, "WFMPRE:XINCR?");
  xzero = vxi11_obtain_double_value(clink, "WFMPRE:XZERO?");
  pt_off = vxi11_obtain_double_value(clink, "WFMPRE:PT_Off?");
  int_ds  = H5Screate(H5S_SCALAR);

  int_attr = H5Acreate2(hdf5_d->file, "timeout_ms", H5T_NATIVE_INT, int_ds, H5P_DEFAULT, H5P_DEFAULT);
  timeout = TIMEOUT;
  ret = H5Awrite(int_attr, H5T_NATIVE_INT, &timeout);
  ret = H5Aclose(int_attr);
  int_attr = H5Acreate2(hdf5_d->file, "n_avgs", H5T_NATIVE_INT, int_ds, H5P_DEFAULT, H5P_DEFAULT);
  ret = H5Awrite(int_attr, H5T_NATIVE_INT, &(acq->n_avgs));
  ret = H5Aclose(int_attr);

  for (i=0; i<acq->n_ch; i++) {
    sprintf(str_buf, "DATA:SOURCE %s", acq->ch_a[i]);
    vxi11_send(clink, str_buf);
    ymult = vxi11_obtain_double_value(clink, "WFMPRE:YMULT?");
    yzero = vxi11_obtain_double_value(clink, "WFMPRE:YZERO?");
    yoff = vxi11_obtain_double_value(clink, "WFMPRE:YOFF?");

    sprintf(str_buf, "%s_ymult", acq->ch_a[i]);
    int_attr = H5Acreate2(hdf5_d->file, str_buf, H5T_NATIVE_DOUBLE, int_ds, H5P_DEFAULT, H5P_DEFAULT);
    ret = H5Awrite(int_attr, H5T_NATIVE_DOUBLE, &ymult);
    ret = H5Aclose(int_attr);

    sprintf(str_buf, "%s_yzero", acq->ch_a[i]);
    int_attr = H5Acreate2(hdf5_d->file, str_buf, H5T_NATIVE_DOUBLE, int_ds, H5P_DEFAULT, H5P_DEFAULT);
    ret = H5Awrite(int_attr, H5T_NATIVE_DOUBLE, &yzero);
    ret = H5Aclose(int_attr);

    sprintf(str_buf, "%s_yoff", acq->ch_a[i]);
    int_attr = H5Acreate2(hdf5_d->file, str_buf, H5T_NATIVE_DOUBLE, int_ds, H5P_DEFAULT, H5P_DEFAULT);
    ret = H5Awrite(int_attr, H5T_NATIVE_DOUBLE, &yoff);
    ret = H5Aclose(int_attr);
  }
  
  int_attr = H5Acreate2(hdf5_d->file, "hor_recordlength", H5T_NATIVE_DOUBLE, int_ds, H5P_DEFAULT, H5P_DEFAULT);
  ret = H5Awrite(int_attr, H5T_NATIVE_DOUBLE, &hor_recordlength);
  ret = H5Aclose(int_attr);
  int_attr = H5Acreate2(hdf5_d->file, "hor_samplerate", H5T_NATIVE_DOUBLE, int_ds, H5P_DEFAULT, H5P_DEFAULT);
  ret = H5Awrite(int_attr, H5T_NATIVE_DOUBLE, &hor_samplerate);
  ret = H5Aclose(int_attr);
  int_attr = H5Acreate2(hdf5_d->file, "xzero", H5T_NATIVE_DOUBLE, int_ds, H5P_DEFAULT, H5P_DEFAULT);
  ret = H5Awrite(int_attr, H5T_NATIVE_DOUBLE, &xzero);
  ret = H5Aclose(int_attr);
  int_attr = H5Acreate2(hdf5_d->file, "hor_interval", H5T_NATIVE_DOUBLE, int_ds, H5P_DEFAULT, H5P_DEFAULT);
  ret = H5Awrite(int_attr, H5T_NATIVE_DOUBLE, &hor_interval);
  ret = H5Aclose(int_attr);
  int_attr = H5Acreate2(hdf5_d->file, "pt_off", H5T_NATIVE_DOUBLE, int_ds, H5P_DEFAULT, H5P_DEFAULT);
  ret = H5Awrite(int_attr, H5T_NATIVE_DOUBLE, &pt_off);
  ret = H5Aclose(int_attr);
  ret = H5Sclose(int_ds);
}

int main(int argc, char *argv[]) {
  int start, i;
  int numBoards;
  float pt, pa, cdelay, ct, rr, delay, dl100_awg_offset, fc, fm, am; 

  int ret;
  int n_avgs;
  long bytes_returned;
  CLINK *clink;
  acq_data *acq;

  hdf5_data *hdf5_d;
  wfm_data *wfm_d; 
  char str_buf[16];

  int nstep, n_wait_reps;
  float cdelay_start, cdelay_stop, cdelay_stepsize;
  float pa_start, pa_stop, pa_stepsize;
  float pt_start, pt_stop, pt_stepsize;
  char fname[256];

  /* User editable pulse blaster definitions. */
  /* Pulse duration. */
  pt = 50*ms;
  /* Pulse amplitude. */
  pa = 1.0; 
  /* Chirp delay. */
  cdelay = 1.0*ms; 
  /* Chirp duration. */
  ct = 1.0*ms;

  /* Rep rate. */
  rr = 5.0*Hz;

  /* FM carrier frequency. */
  fc = 80*MHz;
  /* FM modulation frequency. */
  fm = 10*kHz; 
  /* FM modulation amplitude. */
  am = 0.5*MHz;

  /* User editable data acquisition definitions. */
  char ch[] = "CH1,CH2,CH3,CH4";
  char ip[] = "132.181.41.135";
  //n_avgs = 50;

  /* User editable scan definitions. */
  /* Number of repetitions to wait after the pulse sequence starts before commencing data acquisition. */
  n_wait_reps = 50;

  /* Stepsize for chirp delay scan. */
  cdelay_stepsize = 1.0*ms;
  /* Starting chirp delay time. */
  cdelay_start = 1.0*ms;
  /* Stopping chirp delay time. */
  cdelay_stop = 2.0*ms;

  /* Stepsize for pulse amplitude scan. */
  pa_stepsize = 0.1;
  /* Pulse amplitude scan start. */
  pa_start = 0.1;
  /* Pulse amplitude scan stop. */
  pa_stop = 1.1;

  /* Stepsize for pulse length scan. */
  pt_stepsize = 5*ms;
  /* Pulse length scan start. */
  pt_start = 5*ms;
  /* Pulse length scan stop. */
  pt_stop = 155*ms;

  /* File definitions. */
  int scan_mask[4] = {0, 1, 2, 3};
  /* Channel two is stored as the last waveform, as a no burn pulse reference. */
  int ref_mask[4] = {-1, 4, -1, -1};

#if 1
  n_avgs = 250;
  char filename[] = "09-09-2015_eryso_site2_test.h5";
  
  cdelay_stepsize = 1.0*ms;
  cdelay_start = 1.0*ms;
  cdelay_stop = 2.0*ms;
#else
  n_avgs = 100;
  char filename[] = "09-09-2015_eryso_site1_holewidth.h5";
  
  cdelay_stepsize = 2.0*ms;
  cdelay_start = 1.0*ms;
  cdelay_stop = 21.0*ms;
#endif

  
  //char notes[] = "SHB measurement of Er:YSO, 50 ppm, with 'FTIR magnet', with variable chirp delay. 0.125 Tesla field along D2 axis. E parallel to D2 polarization, spectroscopic site 1 at 659.090 nm in air (Bristol521). Channel information: CH1 - laser reference; CH3 - FP, 300 MHZ FSR; CH4 - DL100 ramp reference; CH5 - transmission without burn pulse. CW burn pulse power: 4.17 mW; CW chirp pulse power: 0.14 mW. Ramp applied to DL100 with HP33120A, 600 mVpp, 500 Hz. Pulse parameters: pulse amplitude = 1.0, pulse time = 50 ms, chirp duration = 1.0 ms, rep rate = 2.0 Hz. No gating AOM.";

  char notes[] = "SHB measurement of Er:YSO, 10 ppm, with 4T magnet. Central wavelength at 659.080 nm in air (Bristol521). E parallel to D2 polarization, spectroscopic site 1. Channel information: CH1 - laser reference; CH3 - FP, 300 MHZ FSR; CH4 - DL100 ramp reference; CH5 - transmission without burn pulse. CW burn pulse power: 2.22 mW; CW chirp pulse power: 0.14 mW. Ramp applied to DL100 with HP33120A, 700 mVpp, 500 Hz. Pulse parameters: pulse amplitude = 1.0, pulse time = 50 ms, chirp duration = 1.0 ms, rep rate = 5.0 Hz. No gating AOM."; //FM spectroscopy measurement; CH2 is the mixed signal (Minicircuits ZFM-3+ mixer) given a 20 MHz reference (applied to EOM) and the transmission signal. EOM modulation amplitude is 0.15. Includes low pass filter on I mixer output (Minicircuits SLP-2.5+).";


  /* Initializing pb. */
  if (init_pb() != 0) {
    return -1;
  }
  else if (calc_pulse_seq_delay(rr, pt, cdelay, ct, &delay, &dl100_awg_offset) != 0) {
    return -1;
  }

  /* Initializing dpo. */
  clink = (CLINK *) malloc(sizeof(CLINK));
  if (clink == 0) {
    printf("malloc failed for clink in main.\n");
    return -1;
  }

  ret = init_dpo_acq(ip, clink);
  if (ret != 0) {
    printf("dpo initialization failed.\n");
    return -1;
  }
  acq = setup_dpo_acq(clink, ch, n_avgs, rr);
  if (acq == 0) {
    free(clink);
    printf("setup_dpo_acq failed in main.\n");
    return -1;
  }



  /* Run chirp delay scan. */ 
#if 0
  /* Setup hdf5 file and allocate wfm data storage. */
  wfm_d = alloc_wfm(5, acq);
  hdf5_d = hdf5_open(filename, wfm_d->n, wfm_d->len);

  nstep = (int) floorf(cdelay_stop-cdelay_start)/cdelay_stepsize;
  cdelay = cdelay_start;
  for (i=0; i<nstep; i++) {
    printf("cdelay=%.1f ms\n",  cdelay/1e6);
    sprintf(str_buf, "%.1fms", cdelay/1e6);

    /* Take reference data with zero pulse amplitude. */
    start_dl100_chirp_pulse_seq(pt, 0, cdelay, ct, rr, delay, dl100_awg_offset);
    sleep(1e-6/rr);

    bytes_returned = run_dpo_acq(clink, acq);
    parse_wfm_buf(acq, bytes_returned, wfm_d, ref_mask);

    /* Take actual holeburning data. */
    start_dl100_chirp_pulse_seq(pt, pa, cdelay, ct, rr, delay, dl100_awg_offset);
    sleep(1e-6/rr);

    bytes_returned = run_dpo_acq(clink, acq);
    parse_wfm_buf(acq, bytes_returned, wfm_d, scan_mask);
    hdf5_write_scan(hdf5_d, str_buf, wfm_d->wfm);

    cdelay += cdelay_stepsize;
  }

  hdf5_write_meta(hdf5_d, clink, acq, notes);
  free_wfm(wfm_d);
  hdf5_close(hdf5_d);
#endif /* cdelay scan enabled. */

  /* Run pulse amplitude scan. */ 
#if 0 
  /* Setup hdf5 file and allocate wfm data storage. */
  wfm_d = alloc_wfm(5, acq);
  hdf5_d = hdf5_open(filename, wfm_d->n, wfm_d->len);
  nstep = (int) floorf((pa_stop-pa_start)/pa_stepsize);
  pa = pa_start;
  for (i=0; i<nstep; i++) {
    printf("pa=%.1f\n",  pa);
    sprintf(str_buf, "%.1f", pa);

    /* Take reference data with zero pulse amplitude. */
    start_dl100_chirp_pulse_seq(pt, 0, cdelay, ct, rr, delay, dl100_awg_offset);
    sleep(1e-6/rr);

    bytes_returned = run_dpo_acq(clink, acq);
    parse_wfm_buf(acq, bytes_returned, wfm_d, ref_mask);

    /* Take actual holeburning data. */
    start_dl100_chirp_pulse_seq(pt, pa, cdelay, ct, rr, delay, dl100_awg_offset);
    sleep(1e-6/rr);

    bytes_returned = run_dpo_acq(clink, acq);
    parse_wfm_buf(acq, bytes_returned, wfm_d, scan_mask);
    hdf5_write_scan(hdf5_d, str_buf, wfm_d->wfm);

    pa += pa_stepsize;
  }

  hdf5_write_meta(hdf5_d, clink, acq, notes);
  free_wfm(wfm_d);
  hdf5_close(hdf5_d);
#endif /* pa scan enabled. */

  /* Run pulse length scan. */ 
#if 0 
  wfm_d = alloc_wfm(5, acq);
  hdf5_d = hdf5_open(filename, wfm_d->n, wfm_d->len);

  nstep = (int) floorf((pt_stop-pt_start)/pt_stepsize);
  pt = pt_start;
  for (i=0; i<nstep; i++) {
    printf("pt=%.1f ms\n",  pt/1e6);
    sprintf(str_buf, "%.1fms", pt/1e6);

    /* Take reference data with zero pulse amplitude. */
    start_dl100_chirp_pulse_seq(pt, 0, cdelay, ct, rr, delay, dl100_awg_offset);
    sleep(1e-6/rr);

    bytes_returned = run_dpo_acq(clink, acq);
    parse_wfm_buf(acq, bytes_returned, wfm_d, ref_mask);

    /* Take actual holeburning data. */
    start_dl100_chirp_pulse_seq(pt, pa, cdelay, ct, rr, delay, dl100_awg_offset);
    sleep(1e-6/rr);

    bytes_returned = run_dpo_acq(clink, acq);
    parse_wfm_buf(acq, bytes_returned, wfm_d, scan_mask);
    hdf5_write_scan(hdf5_d, str_buf, wfm_d->wfm);
    
    pt += pt_stepsize;
  }
  hdf5_write_meta(hdf5_d, clink, acq, notes);
  free_wfm(wfm_d);
  hdf5_close(hdf5_d);

#endif /* pt scan enabled. */

/* FM chirp delay scan. */
#if 0
  /* Setup hdf5 file and allocate wfm data storage. */
  wfm_d = alloc_wfm(5, acq);
  hdf5_d = hdf5_open(filename, wfm_d->n, wfm_d->len);

  nstep = (int) floorf(cdelay_stop-cdelay_start)/cdelay_stepsize;
  cdelay = cdelay_start;
  for (i=0; i<nstep; i++) {
    printf("cdelay=%.1f ms\n",  cdelay/1e6);
    sprintf(str_buf, "%.1fms", cdelay/1e6);

    /* Take reference data with zero pulse amplitude. */
    start_eom_fm_pulse_seq(pt, 0, cdelay, ct, rr, delay, dl100_awg_offset, 0.4);
    sleep(1e-6/rr);

    bytes_returned = run_dpo_acq(clink, acq);
    parse_wfm_buf(acq, bytes_returned, wfm_d, ref_mask);

    /* Take actual holeburning data. */
    start_eom_fm_pulse_seq(pt, pa, cdelay, ct, rr, delay, dl100_awg_offset, 0.4);
    sleep(1e-6/rr);

    bytes_returned = run_dpo_acq(clink, acq);
    parse_wfm_buf(acq, bytes_returned, wfm_d, scan_mask);
    hdf5_write_scan(hdf5_d, str_buf, wfm_d->wfm);

    cdelay += cdelay_stepsize;
  }

  hdf5_write_meta(hdf5_d, clink, acq, notes);
  free_wfm(wfm_d);
  hdf5_close(hdf5_d);
#endif /* cdelay scan enabled. */

  free_dpo_acq(acq);
  vxi11_close_device(ip, clink);
  free(clink);

  start_dl100_chirp_pulse_seq(pt, pa, cdelay, ct, rr, delay, dl100_awg_offset);
  //start_eom_fm_pulse_seq(pt, pa, cdelay, ct, rr, delay, dl100_awg_offset, 0.15);
  //start_aom_fm_pulse_seq(pt, pa, cdelay, ct, rr, delay, dl100_awg_offset, fc, fm, am);

  printf("Continuing will stop program execution\n");
  while(getchar() != '\n');
  pb_stop(); 
  pb_close();


  return 0;
}
